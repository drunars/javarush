package com.javarush.task.task01.task0103;

/* 
Мой юный друг
*/

public class Solution {
    public static void main(String[] args) {
        int x = 3126;
        int y;
        y = 8;
        System.out.println(x - y);
    }
}

/*
Требования:
•	В программе должен использоваться вывод на экран.
•	Выведенный год должен состоять из 4 цифр.
•	Выведенный год должен начинаться с "31".
•	Выведенный год должен соответствовать заданию.
*/