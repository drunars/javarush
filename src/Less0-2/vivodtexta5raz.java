package com.javarush.task.task01.task0102;

/* 
Комплимент учителю
*/

public class Solution {
    public static void main(String[] args) {
        System.out.println("Элли самая красивая");
        System.out.println("Элли самая красивая");
        System.out.println("Элли самая красивая");
        System.out.println("Элли самая красивая");
        System.out.println("Элли самая красивая");
    }
}

/*
Программа должна выводить текст.
•	Текст должен начинаться с "Элли".
•	Текст должен заканчиваться на "красивая".
•	Текст должен состоять из 5 строк.
•	Выводимый текст должен соответствовать заданию
*/