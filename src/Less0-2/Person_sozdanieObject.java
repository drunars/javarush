package com.javarush.task.task01.task0130;

/* 
Наш первый конвертер!
*/

public class Solution {
    public static void main(String[] args) {
        System.out.println(convertCelsiusToFahrenheit(41));
    }

    public static double convertCelsiusToFahrenheit(int celsius) {
        //напишите тут ваш код
        double TF = (9.0/5) * celsius + 32; //1.8 * 41 + 32;

        return TF;
    }
}

/*
TF = (9 / 5) * TC + 32
double TF = (9/5) * celsius + 32; //1.8 * 41 + 32 /делит не правильно
нужно либо так double(9/5) либо просто (9.0/5)
*/