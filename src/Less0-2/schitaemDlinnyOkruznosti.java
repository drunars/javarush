package com.javarush.task.task01.task0129;

/* 
Считаем длину окружности
*/

public class Solution {
    public static void main(String[] args) {
        printCircleLength(5);
    }

    public static void printCircleLength(int radius) {
    	double L = 2 * 3.14 * radius;
    	System.out.println(L);



    }
}

/*
Используем даблв double  в расчетах вместо int, так как тут дробные числа
тобишь числа с плавающей точкой.
L = 2 * Pi * radius = 2 * 3.14 * radius
Length  5 - задана в задаче в методе Мейн ( она идет в radius)
так как это значение радиуса - 5

================
Выведи на экран длину окружности, рассчитанную по формуле: L = 2 * Pi * radius.
Результат - дробное число (тип double).
В качестве значения Pi используй значение 3.14.

Требования:
В методе printCircleLength нужно вывести длину окружности, рассчитанную по формуле: 2 * Pi * radius.
Метод main должен вызывать метод printCircleLength с параметром 5.
Метод main не должен вызывать команду вывода текста на экран.
Программа должна выводить длину окружности с радиусом 5.

*/